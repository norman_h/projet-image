#include <queue>
#include "image_ppm.h"

struct Element {
    int lab[3];
    int i;
    int j;
    int k;
    float dist;

    Element() {
        this->lab[0] = 0;
        this->lab[1] = 0;
        this->lab[2] = 0;

        this->i = 0;
        this->j = 0;
        this->k = 0;
        this->dist = 0;
    }

    Element(int l, int a, int b, int i, int j, int k, float dist) {
        this->lab[0] = l;
        this->lab[1] = a;
        this->lab[2] = b;

        this->i = i;
        this->j = j;
        this->k = k;
        this->dist = dist;
    }

    void afficher() {
        printf("i = %d ; j = %d\n", i, j);
    }
};

void add_update_cluster(CLUSTER C, Element E)
{   
    C->pixels = (unsigned int*) realloc( (void*) C->pixels,  sizeof(unsigned int)*(C->nb_pixels+2));
    C->pixels[C->nb_pixels++] = E.i;
    C->pixels[C->nb_pixels++] = E.j;
    C->update();
}

void tri_bulle_distances(Element* tab, int size)
{
    for (int i = 0; i < size - 1; i++)
        for (int j = 0; j < size - i - 1; j++)
            if (tab[j].dist > tab[j+1].dist) {
                Element temp = tab[j];
                tab[j] = tab[j + 1];
                tab[j + 1] = temp;
            }
}

float calcul_distance(int rgb_j[3], int rgb_k[3], int i_j, int j_j, int i_k, int j_k, float m, float s) {
    float dist_rgb = sqrt(((rgb_k[0] - rgb_j[0]) * (rgb_k[0] - rgb_j[0]))
                        + ((rgb_k[1] - rgb_j[1]) * (rgb_k[1] - rgb_j[1]))
                        + ((rgb_k[2] - rgb_j[2]) * (rgb_k[2] - rgb_j[2])));
    float dist_coord = sqrt(((i_k - i_j) * (i_k - i_j))
                          + ((j_k - j_j) * (j_k - j_j)));
    return sqrt( (dist_coord * dist_coord)/s + (dist_rgb * dist_rgb)/m );
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite1[250], cNomImgEcrite2[250], cNomImgPalette[250];
    int nH, nW, nTaille, KX, KY, Seuil, old_E, E, S;
    float m;

    /********************* LECTURES DES ARGUMENTS *********************/

    if( argc != 9 )
    {
        printf("Mode d'emploi :\n"
               " - ImgIn : image d'entrée\n"
               " - ImgOut1 : image de sortie 1\n"
               " - ImgOut2 : image de sortie 2\n"
               " - ImgPalette : palette couleur de référence\n"
               " - KX : nombre de clusters à l'horizontale\n"
               " - KY : nombre de clusters à la verticale\n"
               " - Seuil : le niveau d'erreur permis\n"
               " - m : dicte la compacité des superpixels\n");
        return 1;
    }

    sscanf(argv[1],"%s",cNomImgLue);
    sscanf(argv[2],"%s",cNomImgEcrite1);
    sscanf(argv[3],"%s",cNomImgEcrite2);
    sscanf(argv[4],"%s",cNomImgPalette);
    sscanf(argv[5],"%d",&KX);
    sscanf(argv[6],"%d",&KY);
    sscanf(argv[7],"%d",&Seuil);
    sscanf(argv[8],"%f",&m);

    /********************* LECTURE ET CREATION DES IMAGES *********************/

    OCTET *ImgIn, *ImgOut1, *ImgOut2, *ImgPalette;
    float* ImgLAB;
    int *ImgMap;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille*3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

    allocation_tableau(ImgOut1, OCTET, nTaille);
    allocation_tableau(ImgOut2, OCTET, nTaille);
    allocation_tableau(ImgPalette, OCTET, 3*(KX * KY));
    allocation_tableau(ImgMap, int, nTaille);
    allocation_tableau(ImgLAB, float, nTaille*3);
    
    for( int i = 0; i < nTaille*3; i += 3 )
        RGB2LAB( ImgIn[i], ImgIn[i+1], ImgIn[i+2], ImgLAB[i], ImgLAB[i+1], ImgLAB[i+2] );

    /********************* ALGORITHME SLIC *********************/

    const int K = KX * KY;
    S = ceil( sqrt( (float)nTaille/(float)K  ) );

    // Initialisation des clusters et de leur centre

    CLUSTER* C = (CLUSTER*) malloc( sizeof( CLUSTER ) * K );
    int idx = 0;
    for( int i = 1; i <= KX; i++ )
        for( int j = 1; j <= KY; j++ )
        {
            C[idx] = cluster::new_cluster( (i*nH)/(KX+1), (j*nW)/(KY+1) );
            C[idx]->perturb( ImgLAB, nW, nH, 3 );
            idx++;
        }

    float s = sqrt((float)nTaille/(float)K);

    std::queue<Element> queue;
    for (int k = 0; k < K; k++) {
        Element new_E = Element(ImgLAB[3 * (C[k]->i * nW + C[k]->j)],
                                ImgLAB[3 * (C[k]->i * nW + C[k]->j) + 1],
                                ImgLAB[3 * (C[k]->i * nW + C[k]->j) + 2],
                                C[k]->i, C[k]->j, k + 1, 0.);
        queue.push(new_E);
    }

    int indVoisin;
    int i_voisin, j_voisin;

    int lab_voisin[3];

    for (int i = 0; i < nTaille; i++) {
        ImgMap[i] = 0;
    }

    // Boucle principale 

    int nb_voisins;
    Element tab_voisins[8];

    while (!queue.empty()) {
        Element temp_E = queue.front();
        queue.pop();
        if (ImgMap[temp_E.i * nW + temp_E.j] == 0) {
            ImgMap[temp_E.i * nW + temp_E.j] = temp_E.k;
            add_update_cluster(C[temp_E.k - 1], temp_E);
            nb_voisins = 0;
            for (int v_i = -1; v_i <= 1; v_i++) {
                for (int v_j = -1; v_j <= 1; v_j++) {
                    i_voisin = temp_E.i + v_i;
                    j_voisin = temp_E.j + v_j;
                    if (!(i_voisin < 0 || i_voisin >= nH || j_voisin < 0 || j_voisin >= nW)) {
                        indVoisin = 3 * (i_voisin * nW + j_voisin);
                        lab_voisin[0] = ImgLAB[indVoisin];
                        lab_voisin[1] = ImgLAB[indVoisin + 1];
                        lab_voisin[2] = ImgLAB[indVoisin + 2];
                        float distance = calcul_distance(lab_voisin, temp_E.lab, i_voisin, j_voisin, temp_E.i, temp_E.j, m, s);
                        Element new_E2 = Element(ImgLAB[indVoisin], ImgLAB[indVoisin+1], ImgLAB[indVoisin],
                                                i_voisin, j_voisin, temp_E.k, distance);
                        if (ImgMap[indVoisin/3] == 0) {                      
                            tab_voisins[nb_voisins] = new_E2;
                            nb_voisins += 1;
                        }
                    }
                }
            }
            tri_bulle_distances(tab_voisins, nb_voisins);
            for (int i = 0; i < nb_voisins; i++)
                queue.push(tab_voisins[i]);
        }
    }

    // /*********************  ECRITURE ET LIBERATION DES DONNEES *********************/

    //get_final_image( ImgIn, ImgOut, nH, nW, C, K );

    if (K < 65536)
        compress_palette_65536(ImgIn, ImgOut1, ImgOut2, ImgPalette, nH, nW, C, K);
    else
        printf("Erreur : nombre de super-pixels supérieur à 65536 !\n");

    ecrire_image_pgm(cNomImgEcrite1, ImgOut1, nH, nW);
    ecrire_image_pgm(cNomImgEcrite2, ImgOut2, nH, nW);
    ecrire_image_ppm(cNomImgPalette, ImgPalette, KX, KY);

    free(ImgIn);
    free(ImgOut1);
    free(ImgOut2);
    free(ImgPalette);
    free(ImgLAB);

    for( int i = 0; i < K; i++ )
        free(C[i]);
    free(C);

    return 0;
}