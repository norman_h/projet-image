#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, KX, KY, Seuil, old_E, E, S;
    float m;

    /********************* LECTURES DES ARGUMENTS *********************/

    if( argc != 7 && argc != 8 )
    {
        printf("Mode d'emploi :\n"
               " - ImgIn : image d'entrée\n"
               " - ImgOut : image de sortie\n"
               " - KX : nombre de clusters à l'horizontale\n"
               " - KY : nombre de clusters à la verticale\n"
               " - Seuil : le niveau d'erreur permis\n"
               " - m : dicte la compacité des superpixels\n"
               " - CompressTxt : fichier txt vers lequel l'image compressée va être écrite ( optionel )\n");
        return 1;
    }

    sscanf(argv[1],"%s",cNomImgLue) ;
    sscanf(argv[2],"%s",cNomImgEcrite);
    sscanf(argv[3],"%d",&KX);
    sscanf(argv[4],"%d",&KY);
    sscanf(argv[5],"%d",&Seuil);
    sscanf(argv[6],"%f",&m);

    /********************* LECTURE ET CREATION DES IMAGES *********************/

    OCTET *ImgIn, *ImgOut;
    float* ImgLAB;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille*3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

    allocation_tableau(ImgOut, OCTET, nTaille*3);
    allocation_tableau(ImgLAB, float, nTaille*3);
    
    for( int i = 0; i < nTaille*3; i += 3 )
        RGB2LAB( ImgIn[i], ImgIn[i+1], ImgIn[i+2], ImgLAB[i], ImgLAB[i+1], ImgLAB[i+2] );

    /********************* ALGORITHME SLIC *********************/

    const int K = KX * KY;
    S = ceil( sqrt( (float)nTaille/(float)K  ) );

    // Initialisation des clusters et de leur centre

    CLUSTER* C = (CLUSTER*) malloc( sizeof( CLUSTER ) * K );
    int idx = 0;
    for( int i = 1; i <= KX; i++ )
        for( int j = 1; j <= KY; j++ )
        {
            C[idx] = cluster::new_cluster( (i*nH)/(KX+1), (j*nW)/(KY+1) );
            C[idx]->perturb( ImgLAB, nW, nH, 3 );
            idx++;
        }

    // Boucle principale 

    do 
    {
        // Ré-initialisation des pixels de chaque cluster 

        for( int k = 0; k < K; k++ )
        {
            free( C[k]->pixels );   
            C[k]->pixels = NULL;
            C[k]->nb_pixels = 0;
        }
            
        // Affetation des pixels dans le meilleur cluster dans un voisinnage de carré de 2S*2S

        for( int i = 0; i < nH; i++ )
            for( int j = 0; j < nW; j++ )
            {
                int new_clust_idx = -1;
                float dist = 1e30;

                for( int k = 0; k < K; k++ )
                {
                    if( C[k]->i-(2*S) <= i && i <= C[k]->i+(2*S)
                     && C[k]->j-(2*S) <= j && j <= C[k]->j+(2*S) )
                    {
                        float cur_dist = C[k]->compare( ImgLAB, i, j, nW, m, S );

                        if( dist > cur_dist )
                        {
                            dist = cur_dist;
                            new_clust_idx = k;
                        }
                    }
                }

                if( new_clust_idx == -1 )
                    printf("Should not happen !\n");
                else 
                {
                    C[new_clust_idx]->pixels = (unsigned int*) realloc( (void*) C[new_clust_idx]->pixels,  sizeof(unsigned int)*(C[new_clust_idx]->nb_pixels+2) );
                    C[new_clust_idx]->pixels[C[new_clust_idx]->nb_pixels++] = i;
                    C[new_clust_idx]->pixels[C[new_clust_idx]->nb_pixels++] = j;
                }
            }
    
        // Mise à jour des cluster et calcul de l'erreur résiduel

        old_E = E;
        E = 0;
    
        for( int k = 0; k < K; k++ )
            E += C[k]->update();

        printf("%d\n", E );
    }
    while( abs(old_E - E )> Seuil );


    /*********************  ECRITURE ET LIBAERATION DES DONNEES *********************/

    get_final_image( ImgIn, ImgOut, nH, nW, C, K );

    ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);

    if( argc == 8 )
        Compress_Txt( ImgOut, argv[7], nH, nW, C, K );

    free(ImgIn);
    free(ImgOut);
    free(ImgLAB);

    for( int i = 0; i < K; i++ )
    {
        free(C[i]->pixels);
        free(C[i]);
    }
    free(C);

    return 0;
}