#include <stdio.h>
#include <tgmath.h>
#include "image_ppm.h"

#include <fstream>

std::ifstream::pos_type filesize(const char* filename)
{
    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    return in.tellg(); 
}

int main(int argc, char* argv[])
{
    if( argc != 5 && argc != 6 && argc != 7 ) 
     {
       printf("Usage: ImageIn1.ppm ImageIn2.ppm CompressImg.txt TargetRes.txt {PSNR_replace}? {filesize_replace}?\n"); 
       exit (1) ;
     }

     if( argc == 6 )
     {
        FILE* f = fopen( argv[4], "a+" );
        if( !f )
            return 1;
            
        fprintf(f, "%f %s\n", ((float)filesize(argv[1]))/((float)filesize(argv[3])), argv[5] );

        fclose(f);
        return 0;
     }

    char cNomImgLue1[250];
    int nH1, nW1;
  
    sscanf(argv[1],"%s",cNomImgLue1) ;
   
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue1, &nH1, &nW1);

    char cNomImgLue2[250];
    int nH2, nW2;
  
    sscanf(argv[2],"%s",cNomImgLue2) ;
   
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue2, &nH2, &nW2);

    if( nH1 != nH2 || nW1 != nW2 )
        return 1; 
  
   int nTaille3 = nH1 * nW1 * 3;
   OCTET *ImgIn1, *ImgIn2;
   allocation_tableau(ImgIn1, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue1, ImgIn1, nH1 * nW1);
   allocation_tableau(ImgIn2, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue2, ImgIn2, nH2 * nW2);

   double EQM = 0.;
	
    for( int i = 0; i < nTaille3; i++)
        EQM += pow(ImgIn1[i]- ImgIn2[i], 2.);

    EQM /= (double) nTaille3;

    double PSNR = 10.*log10(pow(255., 2.)/EQM);
    
    FILE* f = fopen( argv[4], "a+" );
    if( !f )
        return 1;

    if( argc == 7 )	fprintf(f, "%s %f\n", argv[6], PSNR );
    else fprintf(f, "%f %f\n", ((float)filesize(argv[1]))/((float)filesize(argv[3])), PSNR );

    fclose(f);

   free(ImgIn1);
   free(ImgIn2);
   return 1;
}
