#include "stdio.h"
#include <vector>

#define max_size 70000

int main( int argc, char** argv )
{
    if( argc%2 != 0 || argc < 4 )
    {
        printf("Usage : {CDTxt.txt DTxt.txt}+ final.txt\n");
        return 1;
    }

    float FS[max_size], PS[max_size], filesize, PSNR, index, trash;
    int nb_occur[max_size];

    for( int i = 0; i < max_size; i++ )
    {
        FS[i] = PS[i] = 0.;
        nb_occur[i] = 0;
    }
    
    for( int nb_file = 1; nb_file < argc-1; nb_file += 2 )
    {
        FILE* CDTxt = fopen(argv[nb_file], "r");
        FILE* DTxt = fopen(argv[nb_file+1], "r");

        if( !CDTxt )
        {
            printf("Could not open %s\n !", argv[1]);
            return 1;
        }
        else if( !DTxt )
        {
            printf("Could not open %s\n !", argv[2]);
            return 1;
        }

        while( fscanf( CDTxt, "%f %f\n", &filesize, &trash ) != EOF && fscanf( DTxt, "%f %f\n", &index, &PSNR ) != EOF )
        {
            index--;

            FS[(int)index] += filesize;
            PS[(int)index] += PSNR;
            nb_occur[(int)index]++;
        }

        fclose( CDTxt );
        fclose( DTxt );
    }

    FILE* finalFile = fopen(argv[argc-1], "w");
    if( !finalFile )
    {
        printf("Could not open %s\n !", argv[3]);
        return 1;
    }
    
    for( unsigned long i = 0; i < max_size; i++ )
        if( nb_occur[i] )
            fprintf( finalFile, "%f %f\n", FS[i]/(float)nb_occur[i], PS[i]/(float)nb_occur[i] );

    fclose( finalFile );
    return 0;
}

// ./a.out Apple_CDTxt.txt Apple_DTxt.txt Apricot_CDTxt.txt Apricot_DTxt.txt Blackberry_CDTxt.txt Blackberry_DTxt.txt Blackcurrent_CDTxt.txt Blackcurrent_DTxt.txt Cherry_CDTxt.txt Cherry_DTxt.txt Coco_CDTxt.txt Coco_DTxt.txt Lemon_CDTxt.txt Lemon_DTxt.txt Lime_CDTxt.txt Lime_DTxt.txt Mango_CDTxt.txt Mango_DTxt.txt Melon_CDTxt.txt Melon_DTxt.txt Papaya_CDTxt.txt Papaya_DTxt.txt Passionfruit_CDTxt.txt Passionfruit_DTxt.txt Peach_CDTxt.txt Peach_DTxt.txt Pear_CDTxt.txt Pear_DTxt.txt FruitMean.txt
