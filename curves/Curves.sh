# ---------------------------------------------------------------------------------- K parameter 



# ---------------- Pear & Peach ( SLIC & SNIC ) on Mire.ppm ( 512 x 512 ) DONE

for(( i = 2; i <= 130; i += 2 )); do 
    for(( j = i; j <= i+5; j++ )); do 
        echo "$i" "$j" ;
        a=$((i*j)) ; 

        ./SLIC ImgIn/Mire.ppm ImgOut/Pear_{$a}.ppm  $i $j 70 3000 Txt/Pear_{$a}.txt ;
        ./Decompress_Txt Txt/Pear_{$a}.txt ImgOut/Pear_{$a}_DTxt.ppm ;

        ./SNIC ImgIn/Mire.ppm ImgOut/Peach_{$a}.ppm $i $j 3000 Txt/Peach_{$a}.txt ;  
        ./Decompress_Txt Txt/Peach_{$a}.txt ImgOut/Peach_{$a}_DTxt.ppm ;
    done; 
done;  

for(( i = 2; i <= 131*136; i++ )); do
    ./PSNR ImgIn/Mire.ppm ImgOut/Pear_{$i}.ppm  Txt/Pear_{$i}.txt  Curves/Pear.txt 0 $i ;
    ./PSNR ImgIn/Mire.ppm ImgOut/Peach_{$i}.ppm Txt/Peach_{$i}.txt Curves/Peach.txt 0 $i ;

    ./PSNR ImgIn/Mire.ppm ImgOut/Pear_{$i}_DTxt.ppm  Txt/Pear_{$i}.txt  Curves/Pear_DTxt.txt  0 $i ;
    ./PSNR ImgIn/Mire.ppm ImgOut/Peach_{$i}_DTxt.ppm Txt/Peach_{$i}.txt Curves/Peach_DTxt.txt 0 $i ;

    ./PSNR ImgOut/Pear_{$i}.ppm ImgOut/Pear_{$i}_DTxt.ppm   Txt/Pear_{$i}.txt  Curves/Pear_CDTxt.txt ;
    ./PSNR ImgOut/Peach_{$i}.ppm ImgOut/Peach_{$i}_DTxt.ppm Txt/Peach_{$i}.txt Curves/Peach_CDTxt.txt ;
done;

rm Txt/* ImgOut/*



# ---------------- Coco & Cherry ( SLIC & SNIC ) on Teapot.ppm ( 600 x 600 ) DONE

for(( i = 10; i <= 120; i += 2 )); do 
    for(( j = i; j <= i+7; j++ )); do 
        echo "$i" "$j" ;
        a=$((i*j)) ; 

        ./SLIC ImgIn/TeaPot.ppm ImgOut/Coco_{$a}.ppm   $i $j 100 2500 Txt/Coco_{$a}.txt ;  
        ./Decompress_Txt Txt/Coco_{$a}.txt ImgOut/Coco_{$a}_DTxt.ppm ;

        ./SNIC ImgIn/TeaPot.ppm ImgOut/Cherry_{$a}.ppm $i $j 2500 Txt/Cherry_{$a}.txt ; 
        ./Decompress_Txt Txt/Cherry_{$a}.txt ImgOut/Cherry_{$a}_DTxt.ppm ;
    done; 
done; 

for(( i = 10; i <= 121*128; i++ )); do
    ./PSNR ImgIn/TeaPot.ppm ImgOut/Coco_{$i}.ppm   Txt/Coco_{$i}.txt   Curves/Coco.txt 0 $i ;
    ./PSNR ImgIn/TeaPot.ppm ImgOut/Cherry_{$i}.ppm Txt/Cherry_{$i}.txt Curves/Cherry.txt 0 $i ;

    ./PSNR ImgIn/TeaPot.ppm ImgOut/Coco_{$i}_DTxt.ppm   Txt/Coco_{$i}.txt   Curves/Coco_DTxt.txt 0 $i ;
    ./PSNR ImgIn/TeaPot.ppm ImgOut/Cherry_{$i}_DTxt.ppm Txt/Cherry_{$i}.txt Curves/Cherry_DTxt.txt 0 $i ;

    ./PSNR ImgOut/Coco_{$i}.ppm ImgOut/Coco_{$i}_DTxt.ppm     Txt/Coco_{$i}.txt   Curves/Coco_CDTxt.txt ;
    ./PSNR ImgOut/Cherry_{$i}.ppm ImgOut/Cherry_{$i}_DTxt.ppm Txt/Cherry_{$i}.txt Curves/Cherry_CDTxt.txt ;
done;

rm Txt/* ImgOut/*



# ---------------- Mango & Melon ( SLIC & SNIC ) on gradient.ppm ( 612 x 340 ) DONE

for(( i = 10; i <= 64; i += 2 )); do 
    for(( j = i; j <= i+16; j += 2 )); do 
        echo "$i" "$j" ;
        a=$((i*j)) ; 

        ./SLIC ImgIn/gradient.ppm ImgOut/Mango_{$a}.ppm $i $j 30 2000 Txt/Mango_{$a}.txt ; 
        ./Decompress_Txt Txt/Mango_{$a}.txt ImgOut/Mango_{$a}_DTxt.ppm ;

        ./SNIC ImgIn/gradient.ppm ImgOut/Melon_{$a}.ppm $i $j 2000 Txt/Melon_{$a}.txt ; 
        ./Decompress_Txt Txt/Melon_{$a}.txt ImgOut/Melon_{$a}_DTxt.ppm ;
    done; 
done; 

for(( i = 0; i <= 64*81; i++ )); do
    ./PSNR ImgIn/gradient.ppm ImgOut/Mango_{$i}.ppm Txt/Mango_{$i}.txt Curves/Mango.txt 0 $i ;
    ./PSNR ImgIn/gradient.ppm ImgOut/Melon_{$i}.ppm Txt/Melon_{$i}.txt Curves/Melon.txt 0 $i ;

    ./PSNR ImgIn/gradient.ppm ImgOut/Mango_{$i}_DTxt.ppm Txt/Mango_{$i}.txt Curves/Mango_DTxt.txt 0 $i ;
    ./PSNR ImgIn/gradient.ppm ImgOut/Melon_{$i}_DTxt.ppm Txt/Melon_{$i}.txt Curves/Melon_DTxt.txt 0 $i ;

    ./PSNR ImgOut/Mango_{$i}.ppm ImgOut/Mango_{$i}_DTxt.ppm Txt/Mango_{$i}.txt Curves/Mango_CDTxt.txt ;
    ./PSNR ImgOut/Melon_{$i}.ppm ImgOut/Melon_{$i}_DTxt.ppm Txt/Melon_{$i}.txt Curves/Melon_CDTxt.txt ;
done;

rm Txt/* ImgOut/*



# ---------------- Papaya & Passionfruit ( SLIC & SNIC ) on Monarch.ppm ( 512 x 512 ) DONE

for(( i = 5; i <= 250; i += 5 )); do 
    for(( j = i+1; j <= i+5; j++ )); do 
        echo "$i" "$j" ;
        a=$((i*j)) ; 

        ./SLIC ImgIn/Monarch.ppm ImgOut/Papaya_{$a}.ppm       $i $j 150 4000 Txt/Papaya_{$a}.txt ; 
        ./Decompress_Txt Txt/Papaya_{$a}.txt ImgOut/Papaya_{$a}_DTxt.ppm ;

        ./SNIC ImgIn/Monarch.ppm ImgOut/Passionfruit_{$a}.ppm $i $j 4000 Txt/Passionfruit_{$a}.txt ; 
        ./Decompress_Txt Txt/Passionfruit_{$a}.txt ImgOut/Passionfruit_{$a}_DTxt.ppm ;
    done; 
done;  

for(( i = 5; i <= 251*256; i++ )); do
    ./PSNR ImgIn/Monarch.ppm ImgOut/Papaya_{$i}.ppm       Txt/Papaya_{$i}.txt       Curves/Papaya.txt 0 $i ;
    ./PSNR ImgIn/Monarch.ppm ImgOut/Passionfruit_{$i}.ppm Txt/Passionfruit_{$i}.txt Curves/Passionfruit.txt 0 $i ;

    ./PSNR ImgIn/Monarch.ppm ImgOut/Papaya_{$i}_DTxt.ppm       Txt/Papaya_{$i}.txt       Curves/Papaya_DTxt.txt 0 $i ;
    ./PSNR ImgIn/Monarch.ppm ImgOut/Passionfruit_{$i}_DTxt.ppm Txt/Passionfruit_{$i}.txt Curves/Passionfruit_DTxt.txt 0 $i ;

    ./PSNR ImgOut/Papaya_{$i}.ppm ImgOut/Papaya_{$i}_DTxt.ppm       Txt/Papaya_{$i}.txt       Curves/Papaya_CDTxt.txt ;
    ./PSNR ImgOut/Passionfruit_{$i}.ppm ImgOut/Passionfruit_{$i}_DTxt.ppm Txt/Passionfruit_{$i}.txt Curves/Passionfruit_CDTxt.txt ;
done;

rm Txt/* ImgOut/*



# ---------------- Apple & Apricot ( SLIC & SNIC ) on CatLogo.ppm ( 128 x 128 ) DONE

for(( i = 1; i <= 64; i++ )); do 
    for(( j = i; j <= i+5; j++ )); do 
        echo "$i" "$j" ;
        a=$((i*j)) ; 

        ./SLIC ImgIn/CatLogo.ppm ImgOut/Apple_{$a}.ppm $i $j 100 50 Txt/Apple_{$a}.txt ; 
        ./Decompress_Txt Txt/Apple_{$a}.txt ImgOut/Apple_{$a}_DTxt.ppm ;

        ./SNIC ImgIn/CatLogo.ppm ImgOut/Apricot_{$a}.ppm $i $j 50 Txt/Apricot_{$a}.txt ; 
        ./Decompress_Txt Txt/Apricot_{$a}.txt ImgOut/Apricot_{$a}_DTxt.ppm ;
    done; 
done;  

for(( i = 1; i <= 65*70; i++ )); do
    ./PSNR ImgIn/CatLogo.ppm ImgOut/Apple_{$i}.ppm   Txt/Apple_{$i}.txt   Curves/Apple.txt 0 $i ;
    ./PSNR ImgIn/CatLogo.ppm ImgOut/Apricot_{$i}.ppm Txt/Apricot_{$i}.txt Curves/Apricot.txt 0 $i ;

    ./PSNR ImgIn/CatLogo.ppm ImgOut/Apple_{$i}_DTxt.ppm   Txt/Apple_{$i}.txt   Curves/Apple_DTxt.txt 0 $i ;
    ./PSNR ImgIn/CatLogo.ppm ImgOut/Apricot_{$i}_DTxt.ppm Txt/Apricot_{$i}.txt Curves/Apricot_DTxt.txt 0 $i ;

    ./PSNR ImgOut/Apple_{$i}.ppm   ImgOut/Apple_{$i}_DTxt.ppm   Txt/Apple_{$i}.txt   Curves/Apple_CDTxt.txt ;
    ./PSNR ImgOut/Apricot_{$i}.ppm ImgOut/Apricot_{$i}_DTxt.ppm Txt/Apricot_{$i}.txt Curves/Apricot_CDTxt.txt ;
done;

rm Txt/* ImgOut/*


# ---------------- Lemon & Lime ( SLIC & SNIC ) on House.ppm ( 256 x 256 ) DONE

for(( i = 10; i <= 100; i++ )); do 
    for(( j = i; j <= i+5; j++ )); do 
        echo "$i" "$j" ;
        a=$((i*j)) ; 

        ./SLIC ImgIn/House.ppm ImgOut/Lemon_{$a}.ppm $i $j 100 6000 Txt/Lemon_{$a}.txt ; 
        ./Decompress_Txt Txt/Lemon_{$a}.txt ImgOut/Lemon_{$a}_DTxt.ppm ;

        ./SNIC ImgIn/House.ppm ImgOut/Lime_{$a}.ppm  $i $j 6000 Txt/Lime_{$a}.txt ; 
        ./Decompress_Txt Txt/Lime_{$a}.txt ImgOut/Lime_{$a}_DTxt.ppm ;
    done;
done;  

for(( i = 0; i <= 101*106; i++ )); do
    ./PSNR ImgIn/House.ppm ImgOut/Lemon_{$i}.ppm Txt/Lemon_{$i}.txt Curves/Lemon.txt 0 $i ;
    ./PSNR ImgIn/House.ppm ImgOut/Lime_{$i}.ppm  Txt/Lime_{$i}.txt  Curves/Lime.txt 0 $i ;

    ./PSNR ImgIn/House.ppm ImgOut/Lemon_{$i}_DTxt.ppm Txt/Lemon_{$i}.txt Curves/Lemon_DTxt.txt 0 $i ;
    ./PSNR ImgIn/House.ppm ImgOut/Lime_{$i}_DTxt.ppm  Txt/Lime_{$i}.txt  Curves/Lime_DTxt.txt 0 $i ;

    ./PSNR ImgOut/Lemon_{$i}.ppm ImgOut/Lemon_{$i}_DTxt.ppm Txt/Lemon_{$i}.txt Curves/Lemon_CDTxt.txt ;
    ./PSNR ImgOut/Lime_{$i}.ppm  ImgOut/Lime_{$i}_DTxt.ppm  Txt/Lime_{$i}.txt  Curves/Lime_CDTxt.txt ;
done;

rm Txt/* ImgOut/*


# ---------------- Blackberry & Blackcurrent ( SLIC & SNIC ) on Yacht.ppm  ( 512 x 480 ) DONE

for(( i = 20; i <= 150; i ++)); do 
    echo "$i" ;
    a=$((i*i)) ; 

    ./SLIC ImgIn/Yacht.ppm ImgOut/Blackberry_{$a}.ppm   $i $i 200 2500 Txt/Blackberry_{$a}.txt ; 
    ./Decompress_Txt Txt/Blackberry_{$a}.txt ImgOut/Blackberry_{$a}_DTxt.ppm ;

    ./SNIC ImgIn/Yacht.ppm ImgOut/Blackcurrent_{$a}.ppm $i $i 2500 Txt/Blackcurrent_{$a}.txt ; 
    ./Decompress_Txt Txt/Blackcurrent_{$a}.txt ImgOut/Blackcurrent_{$a}_DTxt.ppm ;

    ./PSNR ImgIn/Yacht.ppm ImgOut/Blackberry_{$a}.ppm   Txt/Blackberry_{$a}.txt   Curves/Blackberry.txt 0 $i ;
    ./PSNR ImgIn/Yacht.ppm ImgOut/Blackcurrent_{$a}.ppm Txt/Blackcurrent_{$a}.txt Curves/Blackcurrent.txt 0 $i ;

    ./PSNR ImgIn/Yacht.ppm ImgOut/Blackberry_{$a}_DTxt.ppm   Txt/Blackberry_{$a}.txt   Curves/Blackberry_DTxt.txt 0 $i ;
    ./PSNR ImgIn/Yacht.ppm ImgOut/Blackcurrent_{$a}_DTxt.ppm Txt/Blackcurrent_{$a}.txt Curves/Blackcurrent_DTxt.txt 0 $i ;

    ./PSNR ImgOut/Blackberry_{$a}.ppm   ImgOut/Blackberry_{$a}_DTxt.ppm   Txt/Blackberry_{$a}.txt   Curves/Blackberry_CDTxt.txt ;
    ./PSNR ImgOut/Blackcurrent_{$a}.ppm ImgOut/Blackcurrent_{$a}_DTxt.ppm Txt/Blackcurrent_{$a}.txt Curves/Blackcurrent_CDTxt.txt ;
done; 

rm Txt/* ImgOut/*


# ---------------- Watermelon & Sugar ( SLIC & SNIC ) on Thunder.ppm ( 800 x 527 ) DONE

for(( i = 20; i <= 120; i += 2 )); do 
    for(( j = i; j <= i+32; j += 2 )); do 
        echo "$i" "$j" ;
        a=$((i*j)) ; 

        ./SLIC ImgIn/Thunder.ppm ImgOut/Watermelon_{$a}.ppm $i $j 300 50000 Txt/Watermelon_{$a}.txt ; 
        ./Decompress_Txt Txt/Watermelon_{$a}.txt ImgOut/Watermelon_{$a}_DTxt.ppm ;

        ./SNIC ImgIn/Thunder.ppm ImgOut/Sugar_{$a}.ppm $i $j 50000 Txt/Sugar_{$a}.txt ; 
        ./Decompress_Txt Txt/Sugar_{$a}.txt ImgOut/Sugar_{$a}_DTxt.ppm ;
    done; 
done; 

for(( i = 0; i <= 121*153; i++ )); do
    ./PSNR ImgIn/Thunder.ppm ImgOut/Watermelon_{$i}.ppm Txt/Watermelon_{$i}.txt Curves/Watermelon.txt 0 $i ;
    ./PSNR ImgIn/Thunder.ppm ImgOut/Sugar_{$i}.ppm Txt/Sugar_{$i}.txt Curves/Sugar.txt 0 $i ;

    ./PSNR ImgIn/Thunder.ppm ImgOut/Watermelon_{$i}_DTxt.ppm Txt/Watermelon_{$i}.txt Curves/Watermelon_DTxt.txt 0 $i ;
    ./PSNR ImgIn/Thunder.ppm ImgOut/Sugar_{$i}_DTxt.ppm Txt/Sugar_{$i}.txt Curves/Sugar_DTxt.txt 0 $i ;

    ./PSNR ImgOut/Watermelon_{$i}.ppm ImgOut/Watermelon_{$i}_DTxt.ppm Txt/Watermelon_{$i}.txt Curves/Watermelon_CDTxt.txt ;
    ./PSNR ImgOut/Sugar_{$i}.ppm ImgOut/Sugar_{$i}_DTxt.ppm Txt/Sugar_{$i}.txt Curves/Sugar_CDTxt.txt ;
done;

rm Txt/* ImgOut/*



# ---------------- Dragonfruit & Jackfruit ( SLIC & SNIC ) on Unicorn.ppm ( 1200 x 962 )

for(( i = 30; i <= 330; i += 3 )); do 
    echo "$i" ;
    a=$((i*i)) ; 

    ./SLIC ImgIn/Unicorn.ppm ImgOut/Dragonfruit_{$a}.ppm   $i $i 250 250 Txt/Dragonfruit_{$a}.txt ; 
    ./Decompress_Txt Txt/Dragonfruit_{$a}.txt ImgOut/Dragonfruit_{$a}_DTxt.ppm ;

    ./SNIC ImgIn/Unicorn.ppm ImgOut/Jackfruit_{$a}.ppm $i $i 250 Txt/Jackfruit_{$a}.txt ; 
    ./Decompress_Txt Txt/Jackfruit_{$a}.txt ImgOut/Jackfruit_{$a}_DTxt.ppm ;

    ./PSNR ImgIn/Unicorn.ppm ImgOut/Dragonfruit_{$a}.ppm   Txt/Dragonfruit_{$a}.txt   Curves/Dragonfruit.txt 0 $a ;
    ./PSNR ImgIn/Unicorn.ppm ImgOut/Jackfruit_{$a}.ppm Txt/Jackfruit_{$a}.txt Curves/Jackfruit.txt 0 $a ;

    ./PSNR ImgIn/Unicorn.ppm ImgOut/Dragonfruit_{$a}_DTxt.ppm   Txt/Dragonfruit_{$a}.txt   Curves/Dragonfruit_DTxt.txt 0 $a ;
    ./PSNR ImgIn/Unicorn.ppm ImgOut/Jackfruit_{$a}_DTxt.ppm Txt/Jackfruit_{$a}.txt Curves/Jackfruit_DTxt.txt 0 $a ;

    ./PSNR ImgOut/Dragonfruit_{$a}.ppm   ImgOut/Dragonfruit_{$a}_DTxt.ppm   Txt/Dragonfruit_{$a}.txt   Curves/Dragonfruit_CDTxt.txt ;
    ./PSNR ImgOut/Jackfruit_{$a}.ppm ImgOut/Jackfruit_{$a}_DTxt.ppm Txt/Jackfruit_{$a}.txt Curves/Jackfruit_CDTxt.txt ;
done; 

rm Txt/* ImgOut/*



# ---------------- Strawberry & Starfruit ( SLIC & SNIC ) on UdM.ppm ( 2048 x 2048 ) DO LAST

for(( i = 2; i <= 512; i += 2 )); do 
    for(( j = i; j <= i+32; j += 2 )); do 
        echo "$i" "$j" ;
        a=$((i*j)) ; 

        ./SLIC ImgIn/UdM.ppm ImgOut/Strawberry_{$a}.ppm $i $j 250 3500 Txt/Strawberry_{$a}.txt ; 
        ./Decompress_Txt Txt/Strawberry_{$a}.txt ImgOut/Strawberry_{$a}_DTxt.ppm ;

        ./SNIC ImgIn/UdM.ppm ImgOut/Starfruit_{$a}.ppm  $i $j 3500 Txt/Starfruit_{$a}.txt ; 
        ./Decompress_Txt Txt/Starfruit_{$a}.txt ImgOut/Starfruit_{$a}_DTxt.ppm ;
    done;
done;  

for(( i = 0; i <= 513*545; i++ )); do
    ./PSNR ImgIn/UdM.ppm ImgOut/Strawberry_{$i}.ppm Txt/Strawberry_{$i}.txt Curves/Strawberry.txt 0 $i ;
    ./PSNR ImgIn/UdM.ppm ImgOut/Starfruit_{$i}.ppm  Txt/Starfruit_{$i}.txt  Curves/Starfruit.txt 0 $i ;

    ./PSNR ImgIn/UdM.ppm ImgOut/Strawberry_{$i}_DTxt.ppm Txt/Strawberry_{$i}.txt Curves/Strawberry_DTxt.txt 0 $i ;
    ./PSNR ImgIn/UdM.ppm ImgOut/Starfruit_{$i}_DTxt.ppm  Txt/Starfruit_{$i}.txt  Curves/Starfruit_DTxt.txt 0 $i ;

    ./PSNR ImgOut/Strawberry_{$i}.ppm ImgOut/Strawberry_{$i}_DTxt.ppm Txt/Strawberry_{$i}.txt Curves/Strawberry_CDTxt.txt ;
    ./PSNR ImgOut/Starfruit_{$i}.ppm  ImgOut/Starfruit_{$i}_DTxt.ppm  Txt/Starfruit_{$i}.txt  Curves/Starfruit_CDTxt.txt ;
done;

rm Txt/* ImgOut/*



# ---------------------------------------------------------------------------------- S parameter ( SLIC only )



# ---------------- Banana on iri.ppm ( 573 x 556 ) 

for(( i = 5; i <= 1000; i += 5 )); do 
    echo "$i" ;

    ./SLIC ImgIn/iri.ppm ImgOut/Banana_{$i}.ppm    32 32 $i 1000 Txt/Banana_{$i}.txt ; 
    ./Decompress_Txt Txt/Banana_{$i}.txt ImgOut/Banana_{$i}_DTxt.ppm ;

    ./PSNR ImgIn/iri.ppm ImgOut/Banana_{$i}.ppm    Txt/Banana_{$i}.txt    Curves/Banana.txt 0 $i ;
    ./PSNR ImgIn/iri.ppm ImgOut/Banana_{$i}_DTxt.ppm    Txt/Banana_{$i}.txt    Curves/Banana_DTxt.txt 0 $i ;
    ./PSNR ImgOut/Banana_{$i}.ppm    ImgOut/Banana_{$i}_DTxt.ppm    Txt/Banana_{$i}.txt    Curves/Banana_CDTxt.txt ;
done;

rm Txt/* ImgOut/*


# ---------------- Pineapple on alaska.ppm ( 512 x 512 ) 

for(( i = 10; i <= 1000; i += 10 )); do 
    echo "$i" ;

    ./SLIC ImgIn/alaska.ppm ImgOut/Pineapple_{$i}.ppm   16 16 $i 200 Txt/Pineapple_{$i}.txt ; 
    ./Decompress_Txt Txt/Pineapple_{$i}.txt ImgOut/Pineapple_{$i}_DTxt.ppm ;

    ./PSNR ImgIn/alaska.ppm ImgOut/Pineapple_{$i}.ppm   Txt/Pineapple_{$i}.txt   Curves/Pineapple.txt 0 $i ;
    ./PSNR ImgIn/alaska.ppm ImgOut/Pineapple_{$i}_DTxt.ppm   Txt/Pineapple_{$i}.txt   Curves/Pineapple_DTxt.txt 0 $i ;
    ./PSNR ImgOut/Pineapple_{$i}.ppm   ImgOut/Pineapple_{$i}_DTxt.ppm   Txt/Pineapple_{$i}.txt   Curves/Pineapple_CDTxt.txt ; 
done;

rm Txt/* ImgOut/*




# ---------------------------------------------------------------------------------- m parameter ( SLIC only )


# ---------------- Orange on Iggy.ppm ( 1084 x 1084 ) 

for i in {0..5000..50}; do 
    echo "$i" ;

    ./SLIC ImgIn/Iggy.ppm ImgOut/Orange_{$i}.ppm 64 64 500 $i Txt/Orange_{$i}.txt ; 

    ./Decompress_Txt Txt/Orange_{$i}.txt ImgOut/Orange_{$i}_DTxt.ppm ;

    ./PSNR ImgIn/Iggy.ppm ImgOut/Orange_{$i}.ppm   Txt/Orange_{$i}.txt  Curves/Orange.txt 0 $i ;
    ./PSNR ImgIn/Iggy.ppm ImgOut/Orange_{$i}_DTxt.ppm   Txt/Orange_{$i}.txt  Curves/Orange_DTxt.txt 0 $i ; 
    ./PSNR ImgOut/Orange_{$i}.ppm ImgOut/Orange_{$i}_DTxt.ppm   Txt/Orange_{$i}.txt  Curves/Orange_CDTxt.txt ;  
done;

rm Txt/* ImgOut/*



# ---------------- Kiwi on Phoenix.ppm ( 512 x 512 ) 

for i in {0..3000..25}; do
    echo "$i" ;

    ./SLIC ImgIn/Phoenix.ppm ImgOut/Kiwi_{$i}.ppm 64 64 200 $i Txt/Kiwi_{$i}.txt ; 

    ./Decompress_Txt Txt/Kiwi_{$i}.txt ImgOut/Kiwi_{$i}_DTxt.ppm ;

    # ./PSNR ImgIn/Phoenix.ppm ImgOut/Kiwi_{$i}.ppm Txt/Kiwi_{$i}.txt Curves/Kiwi.txt 0 $i ;
    # ./PSNR ImgIn/Phoenix.ppm ImgOut/Kiwi_{$i}_DTxt.ppm   Txt/Kiwi_{$i}.txt  Curves/Kiwi_DTxt.txt 0 $i ;
    ./PSNR ImgOut/Kiwi_{$i}.ppm ImgOut/Kiwi_{$i}_DTxt.ppm   Txt/Kiwi_{$i}.txt  Curves/Kiwi_CDTxt.txt ;  
done;

rm Txt/* ImgOut/*



# ---------------- Raspberry on Embryos.ppm ( 300 x 300 ) DONE

for i in {0..10000..100}; do
    echo "$i" ;

    ./SLIC ImgIn/Embryos.ppm ImgOut/Raspberry_{$i}.ppm   6 6 40 $i Txt/Raspberry_{$i}.txt ; 

    ./Decompress_Txt Txt/Raspberry_{$i}.txt ImgOut/Raspberry_{$i}_DTxt.ppm ;

    ./PSNR ImgIn/Embryos.ppm ImgOut/Raspberry_{$i}.ppm   Txt/Raspberry_{$i}.txt   Curves/Raspberry.txt 0 $i ;
    ./PSNR ImgIn/Embryos.ppm ImgOut/Raspberry_{$i}_DTxt.ppm   Txt/Raspberry_{$i}.txt  Curves/Raspberry_DTxt.txt 0 $i ;
    ./PSNR ImgOut/Raspberry_{$i}.ppm ImgOut/Raspberry_{$i}_DTxt.ppm   Txt/Raspberry_{$i}.txt  Curves/Raspberry_CDTxt.txt ;  
done;

rm Txt/* ImgOut/*



# ---------------- Nectarine on Galaxy ( 512 x 512 ) DONE


for i in {0..3000..15}; do
    echo "$i" ;

    ./SLIC ImgIn/galaxy.ppm ImgOut/Nectarine_{$i}.ppm 8 8 200 $i Txt/Nectarine_{$i}.txt ; 

    ./Decompress_Txt Txt/Nectarine_{$i}.txt ImgOut/Nectarine_{$i}_DTxt.ppm ;

    ./PSNR ImgIn/galaxy.ppm ImgOut/Nectarine_{$i}.ppm   Txt/Nectarine_{$i}.txt   Curves/Nectarine.txt 0 $i ;
    ./PSNR ImgIn/galaxy.ppm ImgOut/Nectarine_{$i}_DTxt.ppm   Txt/Nectarine_{$i}.txt  Curves/Nectarine_DTxt.txt 0 $i ;
    ./PSNR ImgOut/Nectarine_{$i}.ppm ImgOut/Nectarine_{$i}_DTxt.ppm   Txt/Nectarine_{$i}.txt  Curves/Nectarine_CDTxt.txt ;  
done;

rm Txt/* ImgOut/*



# --- Create all txt files
#for i in Apple Apricot Lemon Lime Blackberry Blackcurrent Strawberry Starfruit Nectarine Raspberry Kiwi Orange Pineapple Pomegranate Banana Blueberry Papaya Passionfruit Mango Melon Cherry Coco Peach Pear Dragonfruit Jackfruit Watermelon Melon ; do 
#touch Curves/$i.txt ;
#touch Curves/"$i"_DTxt.txt ;
#touch Curves/"$i"_CDTxt.txt ;
#done;

# --- remove all created files and empties txt files for curves 
# rm Txt/* ImgOut/* && truncate -s 0 Curves/*


