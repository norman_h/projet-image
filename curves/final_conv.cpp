#include "stdio.h"
#include <vector>

int main( int argc, char** argv )
{
    if( argc != 4 )
    {
        printf("Usage : filesize.txt PSNR.txt final.txt\n");
        return 1;
    }

    FILE* filesize = fopen(argv[1], "r");
    FILE* PSNR = fopen(argv[2], "r");
    FILE* finalFile = fopen(argv[3], "w");

    if( !filesize )
    {
        printf("Could not open %s\n !", argv[1]);
        return 1;
    }
    else if( !PSNR )
    {
        printf("Could not open %s\n !", argv[2]);
        return 1;
    }
    else if( !finalFile )
    {
        printf("Could not open %s\n !", argv[3]);
        return 1;
    }

    std::vector< float > FS;
    std::vector< float > PS;
    float data, trash;
    FS.clear(); PS.clear();

    while( fscanf( filesize, "%f %f\n", &data, &trash ) != EOF )
        FS.push_back(data);
    
    while( fscanf( PSNR, "%f %f\n", &trash, &data ) != EOF )
        PS.push_back(data);

    if( PS.size() != FS.size() )
        printf("There is a problem !\n");
    
    for( unsigned long i = 0; i < PS.size(); i++ )
        fprintf( finalFile, "%f %f\n", FS[i], PS[i] );

    fclose( filesize );
    fclose( PSNR );
    fclose( finalFile );
    return 0;
}

/*
for i in Apple Apricot Lemon Lime Blackberry Blackcurrent Strawberry Starfruit Nectarine Raspberry Kiwi Orange Pineapple Pomegranate Banana Blueberry Papaya Passionfruit Mango Melon Cherry Coco Peach Pear Dragonfruit Jackfruit Watermelon Melon ; do 
./a.out "$i"_CDTxt.txt "$i"_DTxt.txt "$i"_Final.txt 
done;
*/