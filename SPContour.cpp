#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;

    if( argc != 3 ) 
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
        exit (1);
    }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille*3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille*3);

    for (int i=0; i < nH; i++)
        for (int j=0; j < nW; j++)
            {
                bool diff_found = false;
                OCTET cur_R = ImgIn[(i*nW+j)*3];
                OCTET cur_G = ImgIn[(i*nW+j)*3+1];
                OCTET cur_B = ImgIn[(i*nW+j)*3+2];

                for( int k = -1; k <= 1; k++ )
                    for( int l = -1; l <= 1; l++ )
                        if( 0 <= i+k && i+k < nH && 0 <= j+l && j+l < nW )
                        {
                            if( cur_R != ImgIn[((i+k)*nW+(j+l))*3  ]
                             || cur_G != ImgIn[((i+k)*nW+(j+l))*3+1]
                             || cur_B != ImgIn[((i+k)*nW+(j+l))*3+2] )
                                diff_found = true;
                        }
                if( diff_found )
                {
                    ImgOut[(i*nW+j)*3]   = ( 255 - cur_R );
                    ImgOut[(i*nW+j)*3+1] = ( 255 - cur_G );
                    ImgOut[(i*nW+j)*3+2] = ( 255 - cur_B );
                }
                else 
                {
                    ImgOut[(i*nW+j)*3]   = cur_R;
                    ImgOut[(i*nW+j)*3+1] = cur_G;
                    ImgOut[(i*nW+j)*3+2] = cur_B;
                }
            }



    ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn);
    free(ImgOut);
    return 1;
}