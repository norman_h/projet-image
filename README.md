# Projet Image

Ce projet est réalisé dans le cadre des UEs HAI804I - Analyse et traitement des images & HAI809I - Codage et compression multimédia du Master IMAGINE par :
  * HUTTE Norman
  * LIRZIN Léo

# Introduction

L'objectif de ce projet est de compresser des images réelles (issues d'APN) à partir d'une approche de super-pixels.

# Super-pixel

Un super-pixel est défini comme un ensemble de pixels au sein d’une image associés à l’aide de deux propriétés : la distance spatiale et la distance colorimétrique.

# Approches existantes de segmentation

Il existe deux algorithmes régulièrement utilisés pour la segmentation d'une image en super-pixels :
  * SLIC (bon compromis entre efficacité temps/mémoire, n'assure pas lui-même de connexité globale)
  * TurboPixels (exécution plus lente, connexité globale garantie)
Chacun de ces deux algorithmes possède ses avantages et ses défauts. Notre premier objectif sera de les implémenter nous-même dans un premier temps, afin de les comparer et d'en déduire le meilleur pour notre projet.

# Approche possible de compression

Bien que nous étudions encore le sujet de la compression, nous pensons réaliser une compression à base de palette de couleurs, liant alors une image en niveau de gris presentant jusqu'à 256 valeurs possibles, à une palette associant chacune de ces dites valeurs, à une couleur définie.

# -  Bilans hebdomadaires -
Ceux-ci ne sont qu'un court aperçu de nos réalisations explicitées plus amplement au sein des comptes-rendus.

# Semaine 1
    - Choix du sujet
    - Création du diaporama pour justifier notre choix
    - Planification
    - Mise en place du GIT (sur lequel vous vous trouvez)

# Semaine 2
    - Etudes des différentes algorithmes de segmentation possibles

# Semaine 3
    - Implémentation de l'algorithme de segmentation SLIC
    - Implémentation en cours de l'algorithme de segmentation SNIC

# Semaine 4
    - Implémentation complétée de l'algorithme de segmentation SNIC
    - Implémentation d'une compression basée sur une palette de couleurs (max. 256 ou 65536 superpixels)
    - Implémentation d'une compression basée sur les contours des superpixels
    
# Semaine 5
    - Implémentation en cours d'une interface graphique
    - Ecriture de scripts bash et modifications des programmes requises afin de générer des données statistiques
    
# Semaine 6
    - Poursuite de l'implémentation de l'interface graphique
    - Tracé des courbes

# Semaine 7
    - Fin de l'implémentation fonctionnelle de l'interface graphique
    - Fin du tracé et interprétation des courbes

# Semaine 8
    - Fin de l'implémentation esthétique de l'interface graphique
    - Fin de la production et de l'interprétation des courbes
    - Production de la vidéo

# Présentations

Les deux présentations effectuées (choix du sujet et soutenance finale) sont disponibles au sein du dossier "Presentations" de notre répertoire.
La seconde est disponible sous diverses formats :   
    - Sur Google Slides (vidéos intégrées) : https://docs.google.com/presentation/d/1TlT0bO8sUBc7UASQnPVn7efo8zBSigk4ujazEmQGq8M/edit?usp=sharing   
    - Au format PDF (vidéos à part au sein du dossier "Videos") : disponible dans le dossier évoqué
