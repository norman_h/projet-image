#include "image_ppm.h"

#define NB_CALL_REC_LIMIT 50000

void draw_rec( OCTET* ImgOut, OCTET* ImgCtr, OCTET* ImgMark, unsigned short nH, unsigned short nW, unsigned short i, unsigned short j, OCTET R, OCTET G, OCTET B, unsigned short nb_call_rec )
{
    if( nb_call_rec >= NB_CALL_REC_LIMIT )
        return; 

    int idx = i*nW+j;

    if( !ImgMark[idx] || nb_call_rec == 1 ) 
    {
        ImgOut[idx*3  ] = R;
        ImgOut[idx*3+1] = G;
        ImgOut[idx*3+2] = B;

        ImgMark[idx] = 255;

        if( ImgCtr[idx] ) // on est sur un contour, on s'arrête
            return;

        if( i > 0 )
        {
            draw_rec( ImgOut, ImgCtr, ImgMark, nH, nW, i-1, j, R, G, B, nb_call_rec+1 );

            if( j > 0 )
                draw_rec( ImgOut, ImgCtr, ImgMark, nH, nW, i-1, j-1, R, G, B, nb_call_rec+1 );
            
            if( j < nW-1 )
                draw_rec( ImgOut, ImgCtr, ImgMark, nH, nW, i-1, j+1, R, G, B, nb_call_rec+1 );
        }

        if( j > 0 )
            draw_rec( ImgOut, ImgCtr, ImgMark, nH, nW, i, j-1, R, G, B, nb_call_rec+1 );
            
        if( j < nW-1 )
            draw_rec( ImgOut, ImgCtr, ImgMark, nH, nW, i, j+1, R, G, B, nb_call_rec+1 );
        
        if( i < nH-1 )
        {
            draw_rec( ImgOut, ImgCtr, ImgMark, nH, nW, i+1, j, R, G, B, nb_call_rec+1 );

            if( j > 0 )
                draw_rec( ImgOut, ImgCtr, ImgMark, nH, nW, i+1, j-1, R, G, B, nb_call_rec+1 );
            
            if( j < nW-1 )
                draw_rec( ImgOut, ImgCtr, ImgMark, nH, nW, i+1, j+1, R, G, B, nb_call_rec+1 );
        }
    }
}

int fill_in_blank( OCTET* ImgOut, OCTET* ImgMark, OCTET* ImgMarkTmp, int nH, int nW )
{
    int change_happened = 0;

    for( int i = 0; i < nH; i++ )
        for( int j = 0; j < nW; j++ )
            if( ImgMark[(i*nW+j)] == 0 )
            {
                int index = (i*nW+j)*3;

                int col_occur[8];
                OCTET R[8], G[8], B[8];

                OCTET cur_R = ImgOut[index  ];
                OCTET cur_G = ImgOut[index+1];
                OCTET cur_B = ImgOut[index+2];

                int nb_elem = 0;

                for( int k = -1; k <= 1; k++ )
                    for( int l = -1; l <= 1; l++ )
                        if( 0 <= i+k && i+k < nH && 0 <= j+l && j+l < nW && ImgMark[(i+k)*nW+(j+l)] )
                        {
                            bool col_found = false;
                            OCTET cur_R = ImgOut[((i+k)*nW+(j+l))*3];
                            OCTET cur_G = ImgOut[((i+k)*nW+(j+l))*3+1];
                            OCTET cur_B = ImgOut[((i+k)*nW+(j+l))*3+2];

                            for( int m = 0; m < nb_elem; m++ )
                                if( R[m] == cur_R && G[m] == cur_G && B[m] == cur_B )
                                {
                                    col_occur[m]++;
                                    col_found = true;
                                }
                            
                            if( !col_found )
                            {
                                R[nb_elem] = cur_R;
                                G[nb_elem] = cur_G;
                                B[nb_elem] = cur_B;
                                col_occur[nb_elem] = 1;
                                nb_elem++;
                            }
                        }
                
                if( nb_elem == 0 )
                    continue;
                
                int best_col_idx = 0;

                for( int m = 1; m < nb_elem; m++ )
                    if( col_occur[m] > col_occur[best_col_idx] )
                        best_col_idx = m;
                
                ImgOut[index  ] = R[best_col_idx];
                ImgOut[index+1] = G[best_col_idx];
                ImgOut[index+2] = B[best_col_idx];
 
                change_happened = 1;
                ImgMarkTmp[(i*nW+j)] = 255;
            }
    
    return change_happened;
}

int bin2clust( FILE* f, unsigned long& i, unsigned long& j, OCTET& R, OCTET& G, OCTET& B ) 
{
	OCTET log;
	fscanf(f, "%c", &log );

	int log_i = (log>>4)&0xf;
	int log_j = log&0xf;

   static int mm = 0;
   
	i = j = 0;

	unsigned int coeff;

	for( int k = 0, coeff = 0x1; k < log_i; k++, coeff *= 0x100 )
	{
		OCTET i_char;
		fscanf(f, "%c", &i_char );
		i += i_char*coeff;
	}

	for( int k = 0, coeff = 0x1; k < log_j; k++, coeff *= 0x100 )
	{
		OCTET j_char;
		fscanf(f, "%c", &j_char );
		j += j_char*coeff;
    }

    OCTET Rc, Gc, Bc;

    int rv = fscanf( f, "%c%c%c", &Rc, &Gc, &Bc );

    R = Rc;
    G = Gc; 
    B = Bc;

	return rv;
}

int main( int argc, char** argv )
{
    if( argc != 3 && argc != 4 && argc != 5 )
    {
        printf("Usage : Input.txt ImgOut.ppm {Contour.pgm {Blank.ppm}? }?\n");
        return 1;
    }

    FILE* f = fopen( argv[1], "r" );
    if( !f ) 
    {
        printf("Could not open %s !\n", argv[1]);
        return 1;
    }

    int nW, nH, nTaille, K, keep_filling;
    OCTET* str,*ImgOut, *ImgCtr, *ImgMark, *ImgMarkTmp;

    fscanf(f, "%d %d ", &nH, &nW );

    nTaille = nW * nH;

    int size_str = ceil( (float)(nTaille)/8. );
    str = (OCTET*) malloc( sizeof( OCTET ) * size_str );
    memset( str, 0, size_str );

    for( int i = 0; i < size_str; i++ )
        fscanf(f, "%c", &(str[i]) );

    allocation_tableau( ImgOut, OCTET, nTaille*3 );
    allocation_tableau( ImgCtr, OCTET, size_str*8 );
    allocation_tableau( ImgMark, OCTET, nTaille );
    allocation_tableau( ImgMarkTmp, OCTET, nTaille );

    for (int i=0; i < nH; i++)
        for (int j=0; j < nW; j++)
        {
            int idx = i*nW+j;

            if( (str[idx/8])&(1<<(idx%8)) )
                ImgCtr[i*nW+j] = 255;
            else 
                ImgCtr[i*nW+j] = 0;
        }
    
    if( argc >= 4 )
        ecrire_image_pgm(argv[3], ImgCtr, nH, nW);

    long unsigned i, j;
    OCTET R, G, B;

    while( bin2clust(f, i, j, R, G, B ) != EOF )
    {
        draw_rec( ImgOut, ImgCtr, ImgMark, nH, nW, i, j, R, G, B, 1 );
    }
    
    if( argc == 5 )
        ecrire_image_ppm(argv[4], ImgOut, nH, nW);

    for( int i = 0; i < nTaille; i++ )
        ImgMarkTmp[i] = ImgMark[i];

        
    while( fill_in_blank( ImgOut, ImgMark, ImgMarkTmp, nH, nW ) )
    {
        for( int i = 0; i < nTaille; i++ )
            ImgMark[i] = ImgMarkTmp[i];
    }

    ecrire_image_ppm(argv[2], ImgOut, nH, nW);

    free(ImgCtr);
    free(ImgOut);
    fclose(f);
    return 0;
}